#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

#include "catch.hpp"

using namespace std;

string full_name(const string& fn, const string& ln)
{
    return fn + " " + ln;
}

TEST_CASE("reference binding")
{
    SECTION("C++98")
    {
        SECTION("l-value can be bound to T&")
        {
            int x = 10;
            int& rx = x;
        }

        SECTION("r-value can be bound const T&")
        {
            const string& ref_name = full_name("Jan", "Kowalski");
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can be bound with T&&")
        {
            string&& rref_name = full_name("Jan", "Kowalski");
            rref_name.clear();

            REQUIRE(rref_name == "");
        }

        SECTION("l-value can't be bound to T&&")
        {
            string first_name = "Jan";
            // string&& rref_name = first_name; // compiler error
        }
    }
}

TEST_CASE("Moving objects")
{
    string txt1 = "Jan";

    SECTION("copy")
    {
        string txt2 = txt1;

        REQUIRE(txt1 == txt2);
    }

    SECTION("move")
    {
        string txt2 = move(txt1); // explicit move

        REQUIRE(txt2 == "Jan");
        REQUIRE(txt1.size() == 0);  // unsafe - state was moved from txt1
        REQUIRE(txt1 == ""); // unsafe - state was moved from txt1

        SECTION("assignment after move is safe")
        {
            txt1 = full_name("Adam", "Nowak");  // assign
        }
    }
}

template <typename T>
class Container
{
    vector<T> vec_;
public:
    void push_back(const T& item)
    {
        cout << "Copying " << item << " to container..." << endl;
        vec_.push_back(item); // copy
    }

    void push_back(T&& item)
    {
        cout << "Moving " << item << " to container..." << endl;
        vec_.push_back(move(item)); // move
    }

    T& operator[](size_t index)
    {
        return vec_[index];
    }
};

TEST_CASE("Container push_backs")
{
    Container<string> cont;
    string fn = "Jan";

    cont.push_back(fn);
    cont.push_back("Adam");
    cont.push_back(full_name("Zenon", "Nowacki"));

    REQUIRE(cont[2] == "Zenon Nowacki");
}

template <typename T>
class Array
{
    //static_assert(is_arithmetic<T>::value, "T must be a number");

    T* items_;
    size_t size_;
public:
    explicit Array(size_t size) : items_{new T[size]}, size_{size}
    {
        cout << "Array(size: " << size << ")" << endl;
        fill(items_, items_ + size, 0);
    }

    Array(initializer_list<T> lst) : items_{new T[lst.size()]}, size_{lst.size()}
    {
        copy(lst.begin(), lst.end(), items_);
    }

    // copy constructor
    Array(const Array& source) : items_{new T[source.size_]}, size_{source.size_}
    {
        cout << "Array(const Array&)" << endl;
        copy(source.items_, source.items_ + size_, items_);
    }

    // copy assignment operator
    Array& operator=(const Array& source)
    {
        cout << "Array::op=(const Array&)" << endl;

        if (this != &source)
        {
            delete[] items_;
            items_ = new T[source.size_];
            size_ = source.size_;
            copy(source.items_, source.items_ + size_, items_);
        }

        return *this;
    }

    // move constructor
    Array(Array&& source) : items_{source.items_}, size_{source.size_}
    {
        cout << "Array(Array&&)" << endl;
        source.items_ = nullptr;  // important -> destructor will be safely called
        source.size_ = 0; // optional
    }

    // move assignment operator
    Array& operator=(Array&& source)
    {
        cout << "Array::op=(Array&&)" << endl;

        if (this != &source)
        {
            delete[] items_;
            items_ = source.items_;
            size_ = source.size_;

            source.items_ = nullptr; // important -> destructor will be safely called
            source.size_ = 0; // optional
        }

        return *this;
    }

    ~Array() {
        cout << "~Array(size: " << size_ << ")" << endl;
        delete[] items_;
    }

    Array operator*(int value)
    {
        static_assert(is_arithmetic<T>::value, "operator* works only with numbers");

        Array result(*this);

        for(size_t i = 0; i < result.size(); ++i)
            result[i] *= value;

        return result;
    }

    T& operator[](size_t index)
    { return items_[index]; }

    // conversion to int
    operator T() const
    {
        cout << "op int()" << endl;

        return accumulate(items_, items_ + size_, T{});
    }

    // conversion to bool
    explicit operator bool() const
    {
        cout << "op bool()" << endl;

        return !all_of(items_, items_ + size_, [](int v) { return v == T{}; });
    }

    size_t size() const
    { return size_; }
};

template <typename T>
Array<T> operator*(int value, const Array<T>& a)
{
    Array<T> result(a);

    for(size_t i = 0; i < result.size(); ++i)
        result[i] *= value;

    return result;
}

template <typename T>
ostream& operator<<(ostream& out, const Array<T>& a)
{
    out << "Array(size: " << a.size() << ")";
    return out;
}

TEST_CASE("Array")
{
    Array<int> a1(10);

    SECTION("supports copy")
    {
        cout << "\nCoping:\n";

        SECTION("copy ctor")
        {
            Array<int> a2 = a1;

            a1[0] = 1;

            REQUIRE(a1[0] == 1);
            REQUIRE(a2[0] == 0);

            SECTION("copy assignment")
            {
                a1 = a2; // op =
            }
        }
    }

    SECTION("supports move")
    {
        cout << "\nMoving:\n";

        SECTION("move ctor")
        {
            Array<int> a3 = move(a1); // A(A&&)

            REQUIRE(a1.size() == 0);

            SECTION("move assignment")
            {
                Array<int> a4(20);
                a4 = move(a3);

                REQUIRE(a4.size() == 10);
            }
        }
    }
}

Array<int> create_large_array()
{
    Array<int> large_array(1'000'000);

    return large_array;
}

TEST_CASE("Multiplication for array")
{
    Array<int> a = {1, 2, 3};
    Array<int> result = a * 5;

    REQUIRE(result[0] == 5);
    REQUIRE(result[1] == 10);
    REQUIRE(result[2] == 15);

    SECTION("can be converted to int")
    {
        //int sum = result;
    }
}

TEST_CASE("Array can be convered to bool")
{
    cout << "\n\n***************************\n" << endl;

    SECTION("when any item is != 0 -> true")
    {
        Array<int> a = {1, 2, 3};
        REQUIRE(a);

        if (a)
            cout << "a -> true" << endl;
    }

    SECTION("when all items are zero -> false")
    {
        Array<int> b(3);
        REQUIRE(!b);
    }
}

TEST_CASE("Ultimate test")
{
    cout << "\n\n===========================\n" << endl;

    Container<Array<int>> c;

    Array<int> a1 = create_large_array();
    c.push_back(move(a1));
    c.push_back(create_large_array());
}

class Data
{
    string id_;
    Array<int> data_;
public:
    Data(const string& id, size_t size) : id_{id}, data_(size)
    {}

    int sum() const
    {
        return static_cast<int>(data_);
    }
};

TEST_CASE("Moving data")
{
    cout << "\n\n&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n" << endl;

    Data d("data1", 10);

    Data md = move(d);
}

struct Person
{
    const char* name;
};

TEST_CASE("array of people")
{
    // alias
    using People = Array<Person>;

    People people = { Person{"jan"}, Person{"ewa"}, Person{"adam"} };

    //auto result = people * 5;
}

template <typename Key, typename Value>
using MapDescending = std::map<Key, Value, std::greater<Key>>;


namespace MyStdLib
{
    template <typename T>
    struct greater
    {
        bool operator()(const T& a, const T& b) const
        {
            return a > b;
        }
    };
}

template <typename Container>
auto create_view(const Container& c)
{
    using T = typename Container::value_type;

    static_assert(is_same<T, string>::value, "Error");

    vector<const T*> view(c.size());

    transform(c.begin(), c.end(), view.begin(),
              [](const auto& w) { return &w; });

    return view;
}

TEST_CASE("n-th element")
{
    const vector<string> words_original = { "asdfasdf", "fdf", "fd", "sdfsdfsdfsd", "fsdffgadfg", "f" , "fsdfsdf" };

    auto view_words = create_view(words_original);

    nth_element(view_words.begin(), view_words.begin() + 3, view_words.end(),
                [](const string* w1, const string* w2) { return w1->size() < w2->size(); });

    cout << "3 shortest: ";
    for(auto it = view_words.begin(); it != view_words.begin() + 3; ++it)
        cout << **it << " ";
    cout << endl;

}


TEST_CASE("alias of template")
{
    MapDescending<int, string> words_desc = { { 1, "one" }, {3, "three" }, { 4, "four" } };

    for(const auto& kv : words_desc)
    {
        cout << kv.first << " - " << kv.second << endl;
    }

    SECTION("sort with greater")
    {
        vector<int> vec = {6, 23, 23, 3, 6, 235, 654 };

        sort(vec.begin(), vec.end(), greater<int>());
    }
}


