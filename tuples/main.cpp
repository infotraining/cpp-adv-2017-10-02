#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>
#include <numeric>

#include "catch.hpp"

using namespace std;

tuple<int, int, double> calculate_stats(const vector<int>& data)
{
    auto min = *min_element(data.begin(), data.end());
    auto max = *max_element(data.begin(), data.end());
    auto avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(min, max, avg);
}


TEST_CASE("tuples")
{
    SECTION("constructing tuple")
    {
        tuple<int, string, double> t(1, "text", 0.0);

        int fst_item = get<0>(t);

        SECTION("with make_tuple")
        {
            //                                        int, const char*, double
            tuple<int, string, float> t2 = make_tuple(1,   "text",      3.14);
        }
    }

    SECTION("can return many values from function")
    {
        vector<int> vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        tuple<int, int, double> results = calculate_stats(vec);

        SECTION("get<Index>(t) - return i-th item")
        {
            REQUIRE(get<0>(results) == 1);
            REQUIRE(get<1>(results) == 10);

            cout << "Avg: " << get<2>(results) << endl;
        }
    }

    SECTION("tuples with references")
    {
        int x = 10;
        const double dx = 0.01;
        string txt = "txt";

        tuple<int&, const double&, string&> reft(x, dx, txt);

        get<0>(reft) = 20;

        REQUIRE(x == 20);

        SECTION("works with tuples returned from function")
        {
            vector<int> vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            int min, max;
            double avg;

            tuple<int&, int&, double&> results(min, max, avg);
            results = calculate_stats(vec);

            REQUIRE(min == 1);
            REQUIRE(max == 10);
        }

        SECTION("tie creates ref tuple")
        {
            vector<int> vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            int min, max;
            double avg;

            tie(min, max, avg) = calculate_stats(vec);

            SECTION("members can be ignored")
            {
                tie(min, max, ignore) = calculate_stats(vec);
            }
        }

        SECTION("structure bindings - c++17")
        {
            vector<int> vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            //auto[min, max, avg] = calculate_stats(vec);
        }
    }
}

struct Person
{
private:
    auto tied() const
    {
        return tie(last_name, first_name, age);
    }

public:
    string first_name;
    string last_name;
    unsigned char age;

    Person(string fn, string ln, unsigned char age) : first_name{fn}, last_name{ln}, age{age}
    {}

    bool operator<(const Person& other) const
    {
       return tied() < other.tied();
    }

    bool operator>(const Person& other) const
    {
       return tied() > other.tied();
    }

    bool operator>=(const Person& other) const
    {
       return tied() >= other.tied();
    }

    bool operator==(const Person& other) const
    {
        return tied() == other.tied();
    }

    bool operator!=(const Person& other) const
    {
        return tied() != other.tied();
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person{" << p.last_name << ", " << p.first_name << ", " << static_cast<int>(p.age) << "}";
    return out;
}


TEST_CASE("sorting people")
{
    vector<Person> vec = { Person{"jan", "kowalski", 44}, Person{"adam", "nowak", 66},
                           Person{"jan", "kowalski", 23}, Person{"ewa", "adamska", 66} };

    sort(vec.begin(), vec.end()); // deafult sort

    REQUIRE(is_sorted(vec.begin(), vec.end()));

    for(const auto& item : vec)
    {
        cout << item << " ";
    }
    cout << endl;


    SECTION("TODO - sorting by age")
    {
        stable_sort(vec.begin(), vec.end(), [](const Person& p1, const Person& p2) { return p1.age <  p2.age; });
    }

    SECTION("TODO - sorting using pointers")
    {
        vector<const Person*> ptrs;
        vector<int> bollocks;

        transform(vec.begin(), vec.end(), back_inserter(ptrs), [](const Person& p) { return &p; });

        sort(vec.begin(), vec.end(), [](const Person* p1, const Person* p2) { return p1->age < p2->age; });

        for(const Person* p : ptrs)
        {
            cout << *p << " ";
        }
        cout << endl;
    }
}

struct X
{
    int a, b;
};


TEST_CASE("universal initialization")
{
    SECTION("C++98")
    {
        X x = { 1, 2 };
        int tab[8] = { 1, 2, 3, 4 };
        Person p("adam", "nowak", 44);

        vector<int> vec;
        vec.push_back(1);
        vec.push_back(1);

        int v = int();
        //int w();  // most vexing parse error
    }

    SECTION("C++11")
    {
        X x = {1, 2};
        int tab[8] = { 1, 2, 3, 4 };
        Person p{"adam", "nowak", 44}; // calling a constructor
        vector<int> vec = { 1, 2, 3, 4, 5 };
        int v = int{};
        int w{}; // value 0 is set to variable

        unsigned long long ux = 3453223;

        //short s{46378426};  // error - compiler checks overflow

        SECTION("be carefull with containers")
        {
            vector<int> vec1(10, 1);  // { 1, 1, 1, 1, 1, 1, ... }

            vector<int> vec2{10, 1}; // {10, 1}
        }
    }
}

