#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <type_traits>

#include "catch.hpp"

using namespace std;

struct Person
{
private:
    auto tied() const
    {
        return tie(last_name, first_name, age);
    }

public:
    string first_name;
    string last_name;
    unsigned char age;

    Person(string fn, string ln, unsigned char age) : first_name{fn}, last_name{ln}, age{age}
    {}

    bool operator<(const Person& other) const
    {
       return tied() < other.tied();
    }
};

template <typename T>
const T& maximum(const T& a, const T& b)
{
    return (a < b) ? b : a;
}

// specialization for const char*
const char* maximum(const char* a, const char* b)
{
    return (strcmp(a, b) > 0) ? a : b;
}

TEST_CASE("maximum")
{
    REQUIRE(maximum(1, 7) == 7);

    REQUIRE(maximum(3.14, 8.33) == 8.33);

    string txt1 = "Ola";
    string txt2 = "Ala";

    REQUIRE(maximum(txt1, txt2) == "Ola");

    Person p1{"jan", "kowalski", 33};
    Person p2{"adam", "nowak", 23 };

    auto& result = maximum(p1, p2);

    REQUIRE(result.last_name == "nowak");

    SECTION("resolve ambigous deduction")
    {
        REQUIRE(maximum(static_cast<double>(1), 3.14) == Approx(3.14));

        REQUIRE(maximum<double>(1, 3.14) == Approx(3.14));
    }

    SECTION("const char*")
    {
        REQUIRE(strcmp(maximum("Ola", "Ala"), "Ola") == 0);
    }
}

template <typename Container>
void print(const Container& container)
{
    // foreach(item in container)
    //    do(item)

    for(const auto& item : container)
        cout << item << " ";
    cout << endl;

    //SECTION("foreach is interpreted by compiler as")
    //{
    //    for(auto it = container.begin(); it != container.end(); ++it)
    //    {
    //        const auto& item = *it;
    //        cout << item << " ";
    //    }
    //}
}

TEST_CASE("print")
{
    vector<int> v = { 1, 2, 3 };
    print(v);

    vector<string> s = { "ala", "ola", "ewa" };
    print(s);

    list<string> ls = { "bentley", "ferrari" };
    print(ls);

    int numbers[10] = { 1, 2, 3 };
    print(numbers);
}

bool is_even(int x)
{
    return x % 2 == 0;
}

template <typename Iterator, typename Predicate>
Iterator my_find_if(Iterator first, Iterator last, Predicate pred)
{
    for(auto it = first; it != last; ++it)
    {
        if (pred(*it))
            return it;
    }

    return last;
}

TEST_CASE("my_find_if")
{
    vector<int> vec = { 1, 3, 5, 4, 6, 7 };

    SECTION("works with function predicate")
    {
        auto result = my_find_if(vec.begin(), vec.end(), &is_even);
        REQUIRE(*result == 4);

        REQUIRE(count_if(vec.begin(), vec.end(), &is_even) == 2);
    }

    SECTION("works with lambda predicate")
    {
        auto result = my_find_if(vec.begin(), vec.end(), [](int x) { return x % 2 == 0;});
        REQUIRE(*result == 4);
    }

    SECTION("when nothing is found end() is returned")
    {
        list<int> lst = { 1, 3, 5 };

        REQUIRE(my_find_if(lst.begin(), lst.end(), &is_even) == lst.end());
    }
}

template <typename T>
ostream& operator<<(ostream& out, const vector<T>& container)
{
    out << "{ ";
    for(const auto& item : container)
        out << item << " ";
    out << "}";

    return out;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const list<T>& container)
{
    out << "{ ";
    for(const auto& item : container)
        out << item << " ";
    out << "}";

    return out;
}

template <typename T1, typename T2, typename Enabled = void>
struct Pair
{
    T1 first;
    T2 second;

    Pair(const T1& fst, const T2& snd) : first{fst}, second{snd}
    {
    }

    void print() const
    {
        cout << "Pair<T1, T2>{" << first << ", " << second << "}" << endl;
    }
};

// partial specialization of template Pair<T1, T2>
template <typename T>
struct Pair<T, T, enable_if_t<!is_pointer<T>::value>>
{
    T first, second;

    void print() const
    {
        cout << "Pair<T, T>{" << first << ", " << second << "}" << endl;
    }
};

// partial specialization of template Pair<T1, T2>
template <typename T1, typename T2>
struct Pair<T1*, T2*>
{
    T1* first;
    T2* second;

    void print() const
    {
        cout << "Pair<T1*, T2*>{" << (*first) << ", " << (*second) << "}" << endl;
    }
};

// final specialization
template <>
struct Pair<const char*, const char*>
{
    const char* first;
    const char* second;

    void print() const
    {
        cout << "Pair<const char*, const char*>{" << first << ", " << second << "}" << endl;
    }

    const char* max() const
    {
        return (strcmp(first, second) > 0) ? first : second;
    }
};

TEST_CASE("class template")
{
    Pair<int, string> p1{1, "one"};
    p1.print();

    REQUIRE(p1.first == 1);
    REQUIRE(p1.second == "one");

    Pair<double, double> p2{3.14, 7.88};
    p2.print();

    REQUIRE(maximum(p2.first, p2.second) == Approx(7.88));

    Pair<vector<int>, list<int>> p3{ {1, 2, 3}, {4, 5, 6} };
    p3.print();

    SECTION("specialization for class templates")
    {
        Pair<int, int> p4{4, 6};
        p4.print();

        //double pi = 3.14;

        Pair<int*, int*> p5;
        p5.first = &p4.first;
        p5.second = &p4.second;
        p5.print();

        Pair<const char*, const char*> p6{"ola", "ala"};
        p6.print();

        REQUIRE(p6.max() == "ola");
    }
}
