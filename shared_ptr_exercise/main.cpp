#include <cassert>
#include <cstdlib>
#include <iostream>
#include <set>
#include <stdexcept>
#include <memory>

class Observer
{
public:
    virtual void update(const std::string& event_args) = 0;
    virtual ~Observer() {}
};

class Subject
{
    using WeakPtrObserver = std::weak_ptr<Observer>;
    using WeakPtrComparer = std::owner_less<std::weak_ptr<Observer>>;

    int state_;
    std::set<WeakPtrObserver, WeakPtrComparer> observers_;
public:
    Subject() : state_(0)
    {
    }

    void register_observer(WeakPtrObserver observer)
    {
        observers_.insert(observer);
    }

    void unregister_observer(WeakPtrObserver observer)
    {
        observers_.erase(observer);
    }

    void set_state(int new_state)
    {
        if (state_ != new_state)
        {
            state_ = new_state;
            notify("Changed state on: " + std::to_string(state_));
        }
    }

protected:
    void notify(const std::string& event_args)
    {
        for (WeakPtrObserver observer : observers_)
        {
            auto current_observer = observer.lock();
            if (current_observer)
                current_observer->update(event_args);
        }
    }
};

class ConcreteObserver1 : public Observer
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver1: " << event << std::endl;
    }
};

class ConcreteObserver2 : public Observer
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver2: " << event << std::endl;
    }
};

int main(int argc, char const* argv[])
{
    using namespace std;

    Subject s;

    auto o1 = make_shared<ConcreteObserver1>();
    s.register_observer(o1);

    {
        auto o2 = make_shared<ConcreteObserver2>();
        s.register_observer(o2);

        s.set_state(1);        

        cout << "End of scope." << endl;
    }

    s.set_state(2);
}
