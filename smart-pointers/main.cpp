#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <thread>
#include <future>

using namespace std;
using namespace Catch::Matchers;

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
public:
    Gadget(int id,const string& name) :id_{id}, name_{name}
    {
        cout << "Gadget(" << id << ", " << name_ << ")" << endl;
    }

    Gadget() = default;

    Gadget(Gadget&&) = default;
    Gadget& operator=(Gadget&&) = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    virtual void info() const
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")\n";
    }

    virtual void use() const
    {
        cout << "using Gadget(" << id_ << ", " << name_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

namespace Legacy
{
    Gadget* get_gadget(const string& name)
    {
        static int id;
        return new Gadget(++id, name);
    }

    void use(Gadget* g)
    {
        if (g)
            g->use();                        
    }

    void use_and_destroy(Gadget* g)
    {
        if (g)
            g->use();

        delete g;
    }

    Gadget* create_gadgets()
    {
        Gadget* tab = new Gadget[10];

        return tab;
    }

    void destroy(Gadget* g)
    {
        delete g;
    }
}

std::unique_ptr<Gadget> create_gadget(const string& name)
{
    static int id = 999;
    return make_unique<Gadget>(++id, name);
}

void use(Gadget* g)
{
    if (g)
        g->use();
}

void use(unique_ptr<Gadget> g)
{
    if (g)
        g->use();
} // g is destroyed



TEST_CASE("Leaky code")
{
    Gadget* g = Legacy::get_gadget("ipad");

    g->use();

    Legacy::use(Legacy::get_gadget("smartwatch")); // leak

    Legacy::use(g);

    g->info(); // ub - dangling pointer

    Gadget* gs = Legacy::create_gadgets();

    delete[] gs;
}

TEST_CASE("Modern C++")
{
    unique_ptr<Gadget> g = create_gadget("mp3 player");

    g->use();

    Legacy::use(g.get());

    use(move(g)); // explicit move

    use(create_gadget("mp5 player")); // implicit move

    SECTION("can be released to sink functions")
    {
        unique_ptr<Gadget> g2 = create_gadget("mp4 player");
        Legacy::use_and_destroy(g2.release());
    }
}

TEST_CASE("unique_ptr controls lifetime of object")
{
    SECTION("C++11")
    {
        unique_ptr<Gadget> g(new Gadget{13, "ipad2"});
        g->info();
        (*g).info();
    }

    SECTION("C++14")
    {
        unique_ptr<Gadget> g = make_unique<Gadget>(14, "ipad3");
        g->info();
        (*g).info();
    }
}

TEST_CASE("unique_ptr")
{
    SECTION("is moveable-only")
    {
        auto ptr_g = make_unique<Gadget>(14, "ipad665");
        unique_ptr<Gadget> other_ptr = move(ptr_g);

        REQUIRE(ptr_g == nullptr);
        REQUIRE(other_ptr->id() == 14);

        other_ptr->info();
    }

    SECTION("get() returns raw-pointer to an object")
    {
        auto ptr_g = make_unique<Gadget>(15, "ipad666");

        Legacy::use(ptr_g.get());
    }

    SECTION("release()")
    {
        auto ptr_g = make_unique<Gadget>(16, "ipad667");

        Legacy::destroy(ptr_g.release());

        REQUIRE(ptr_g == nullptr);
    }

    SECTION("assignment deletes previous object")
    {
        auto ptr_g = create_gadget("hipster stuff");
        ptr_g->use();

        ptr_g = create_gadget("hipster extra stuff");
    }

    SECTION("can be stored in container")
    {
        vector<std::unique_ptr<Gadget>> gadgets;

        gadgets.push_back(create_gadget("i1"));
        gadgets.push_back(create_gadget("i2"));

        unique_ptr<Gadget> g = create_gadget("i3");
        gadgets.push_back(move(g));

        for(const auto& gd : gadgets)
        {
            gd->use();
        }
    }
}

TEST_CASE("unique_ptr<T[]> handles dynamic arrays")
{
    unique_ptr<Gadget[]> gadgets(Legacy::create_gadgets());

    gadgets[0].use();
}

TEST_CASE("shared_ptrs")
{
    map<string, shared_ptr<Gadget>> dict;

    {
        shared_ptr<Gadget> g1 = make_shared<Gadget>(777, "phone");
        REQUIRE(g1.use_count() == 1);
        REQUIRE(g1.unique());

        shared_ptr<Gadget> g2 = g1; // copy
        REQUIRE(g1.use_count() == 2);
        REQUIRE(!g1.unique());

        dict.insert(make_pair(g1->name(), g1)); // copy into map
        REQUIRE(g1.use_count() == 3);
    } // g2 & g1 are destroyed

    REQUIRE(dict["phone"].use_count() == 1);

    dict["phone"]->use();
} // phone is destroyed

using LookupTable = map<string, string>;

struct TestLookupTable : public LookupTable
{
public:
    using LookupTable::LookupTable;

    ~TestLookupTable()
    {
        cout << "~LookupTable" << endl;
    }
};

void download_content(shared_ptr<const TestLookupTable> dns, const string& name, string& result)
{
    result = "not-found";

    if (dns->count(name))
    {
        cout << "Downloading content from: " << dns->at(name) << endl;
        this_thread::sleep_for(chrono::milliseconds(100 * name.size()));
        result = "Content of " + name;
    }

    //return result;

    cout << "end of download " << name <<endl;
}

string download_content_alt(shared_ptr<const TestLookupTable> dns, const string& name)
{
    string result = "not-found";

    this_thread::sleep_for(chrono::milliseconds(100 * name.size()));
    cout << "Downloading content from: " << dns->at(name) << endl;
    result = "Content of " + name;

    cout << "end of download " << name <<endl;

     return result;
}

TEST_CASE("shared_ptr in multithreading")
{
    initializer_list<pair<const string, string>> lst
            = { pair<const string, string>{ "localhost", "128.0.0.1"} ,
                pair<const string, string>{"wabco.coooooooooooooooooooom", "32.43.111.255"} };

    shared_ptr<const TestLookupTable> lookup_table =
        make_shared<const TestLookupTable>(lst);

    SECTION("shared_ptr with futures")
    {
        future<string> f1 =
                async(launch::async, [=] { return download_content_alt(lookup_table, "localhost"); });

        future<string> f2 =
                async(launch::async, [&] { return download_content_alt(lookup_table, "google.com"); });

        while(f1.wait_for(100ms) != future_status::ready)
        {
            cout << ".";
            cout.flush();
        }
        cout << endl;

        try
        {
            cout << f1.get() << endl;
            cout << f2.get() << endl;
        }
        catch (const out_of_range& e)
        {
            cout << "Caught an exception: " << e.what() << endl;
        }
    }


    SECTION("shared_ptr in threads")
    {
        string r1, r2;

        thread thd1(&download_content, lookup_table, "localhost", ref(r1));
        thread thd2(&download_content, lookup_table, "wabco.coooooooooooooooooooom", ref(r2));

        lookup_table.reset();

        thd1.join();
        thd2.join();

        cout << r1 << endl;
        cout << r2 << endl;
    }
}
