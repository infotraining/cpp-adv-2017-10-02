#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

#include "catch.hpp"

using namespace std;

struct Lambda_2378462789349273
{
    void operator()() const
    {
        cout << "Hello, I'm lambda" << endl;
    }
};

TEST_CASE("lambdas")
{
    auto l = [](){ cout << "Hello, I'm lambda" << endl; };

    l();
    l();
    l();

    SECTION("can return value")
    {
        auto gen = []() { return 42; };

        REQUIRE(gen() == 42);
    }

    SECTION("can have arguments")
    {
        auto square = [](int x) { return x * x; };

        REQUIRE(square(4) == 16);
        REQUIRE(square(10) == 100);
    }

    SECTION("use case")
    {
        vector<int> vec = { 1, 2, 3, 4, 5, 6, 7, 8 };

        transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x; });

        for(const auto& item : vec)
            cout << item << " ";
        cout << endl;

        SECTION("with second vector")
        {
            vector<int> vec2 = { 1, -1, 0, 2, 0, 1, -1, 3 };

            int results[8];

            transform(vec.begin(), vec.end(), vec2.begin(), begin(results), [](int x, int y) { return x * y; });

            for(const auto& item : results)
                cout << item << " ";
            cout << endl;

            SECTION("partition_copy")
            {
                vector<int> evens;
                vector<int> odds;

               partition_copy(vec.begin(), vec.end(), back_inserter(evens), back_inserter(odds),
                              [](int x) { return x % 2 == 0; });

               cout << "Evens: ";
               for(const auto& item : evens)
                   cout << item << " ";
               cout << endl;

               cout << "Odds: ";
               for(const auto& item : odds)
                   cout << item << " ";
               cout << endl;
            }
        }
    }
}

struct Lambda_with_ref_capture_823482
{
    double& sum_;

    Lambda_with_ref_capture_823482(double& sum) : sum_{sum}
    {}

    void operator()(int x) const { sum_ += x; }
};

struct Lambda_with_value_capture_823481
{
    double avg_;

    Lambda_with_value_capture_823481(double avg) : avg_{avg}
    {}

    bool operator()(int x) const { return x > avg_; }
};

TEST_CASE("Captures & closures")
{
    vector<int> vec = { 1, 2368, 423 , 234, 1, 234, 453, 2342, 523, 542 };

     double sum = 0.0;

     for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x; });  // capture by ref

    auto avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

    auto first_gt_avg = find_if(vec.begin(), vec.end(), [avg](int x) { return x > avg; }); // capture by value

    cout << "First item gt avg: " << *first_gt_avg << endl;

    SECTION("default options")
    {
        int x, y, z;

        x = y = z = 1;

        SECTION("all can be captured by value")
        {
            auto l1 = [=] { return x * y * z; };

            x = 100;

            REQUIRE(l1() == 1);
        }

        SECTION("all can be captured by ref")
        {
            auto l2 = [&] { return x * y * z; };

            x = 100;

            REQUIRE(l2() == 100);
        }
    }
}
